package org.eaves.mobility.cli;

import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;
import org.eaves.mobility.data.CSVGoogleMobilityStore;
import org.eaves.mobility.data.Location;
import org.eaves.mobility.data.MobilityDataStore;
import org.eaves.mobility.data.Segment;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Mobility
{

    
    private Map<String, MobilityDataStore> _reports = new HashMap<>();
    private Map<String, Segment> _segments = new HashMap<>();
    
    private MobilityDataStore _currentReport;


    private Mobility() {

    }

    @Command
    public String use(String report)
    {
        MobilityDataStore mds = _reports.get(report);
        String rv = "No report ["+report+"] found";
        if (mds == null) 
        {
            return rv;
        }
        _currentReport = mds;

        return "Using report ["+report+"]";
    }
    
    @Command
    public String load(String file, String report)
    {
        String fullFile = file+"_AU_Region_Mobility_Report.csv";
        MobilityDataStore mds = new CSVGoogleMobilityStore(fullFile, report);
        _reports.put(report, mds);
        use(report);
        return "Loaded: " + fullFile + " Into ["+ report +"]";
    }
    
    @Command
    public String segment(String name, String placeId, String start, String end)
    {
        Location place = _currentReport.place(placeId);
        Segment s = _currentReport.segment(place, start, end);
        String rv = "Segment not created";
        
        if (s != null)
        {
            _segments.put(name, s);
            rv = "Segment created [ " + name + " ]";
        }
        return rv;
    }
    
    @Command
    public String locations(String someMatch) {
        String globbit = ".*" + someMatch + ".*";
        
        List<String> locations = _currentReport.locations().stream().filter(item -> item.matches(globbit)).
                map(item -> item.fullLocation()).collect(Collectors.toList());
        return String.join("\n", locations);
    }
    
    @Command 
    public String mobility(String segment) {
        
        Segment s = _segments.get(segment);
        return s.toString();
    }
    
    @Command
    public String compare(String compared, String baseline)
    {
        Segment b = _segments.get(baseline);
        Segment c = _segments.get(compared);
        
        return c.compareTo(b);
    }

    @Command
    public void quit() {
        System.exit(0);
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Starting mobility choice CLI");

        ShellFactory.createConsoleShell(">> ", "", new Mobility())
                .commandLoop();
    }
    
}
