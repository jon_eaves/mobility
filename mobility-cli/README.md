Understanding the voting preferences of a location
based on the first preferences of the residents.

This can be used to create a voting sentiment.

###Commands

* c	compare	(p1, p2)
* l	load	(p1, p2)
* u	use	(p1)
* s	segment	(p1, p2, p3, p4)
* m	mobility	(p1)
* q	quit	()
* l	locations	(p1)

### CLI Usage

     >> > load 2021 year21
     LineCount :55432
     Loaded: 2021_AU_Region_Mobility_Report.csv Into [year21]
     >> > locations Mel
     Victoria / Melton City / ChIJAy6X3ojy1moREPMohCgh1GM
     Western Australia / City of Melville / ChIJSdDUKyGjMioR6_oxNP9V-Vw
     Victoria / Melbourne City / ChIJv_FYgkNd1moRpxLuRXZURFs
     >> > locations Syd
     New South Wales / North Sydney Council / ChIJs7S7tOyuEmsRpUU5oJ3BdDM
     New South Wales / Council of the City of Sydney / ChIJl9aAttixEmsR8d2wSrqVi5k
     >> > segment mel_l_4 ChIJv_FYgkNd1moRpxLuRXZURFs 26-05-2021 10-06-2021
     Segment created [ mel_l_4 ]
     >> > segment mel_pre_l_4 ChIJv_FYgkNd1moRpxLuRXZURFs 26-03-2021 26-04-2021
     Segment created [ mel_pre_l_4 ]
     >> > mobility mel_pre_l_4
     [ Victoria / Melbourne City / ChIJv_FYgkNd1moRpxLuRXZURFs ] (2021-03-26 - 2021-04-26) Moving: 66.72 / Retail 66.16 / Home 109.47
     >> > mobility mel_l_4
     [ Victoria / Melbourne City / ChIJv_FYgkNd1moRpxLuRXZURFs ] (2021-05-26 - 2021-06-10) Moving: 36.36 / Retail 22.25 / Home 127.81
     >> > compare mel_l_4 mel_pre_l_4
     Moving: -45.50% / Retail -66.37% / Home 16.76%
    
        










