package org.eaves.mobility.data;

import java.util.List;

public interface MobilityDataStore
{
    Segment segment(Location location, String startDate, String endDate);

    List<Location> locations();

    Location location(String nsw, String waveryley_council);

    Location place(String placeId);
}
