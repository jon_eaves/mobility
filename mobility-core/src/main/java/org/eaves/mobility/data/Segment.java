package org.eaves.mobility.data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class Segment
{
    private final Location _location;
    private final List<Telemetry> _telemetry;

    public Segment(Location location, List<Telemetry> telemetry)
    {
        _location = location;
        _telemetry = telemetry;
    }
    
    public double movement()
    {
        return _telemetry.stream().mapToInt((x) -> x.movement()).summaryStatistics().getAverage() / 5;
    }

    public double home()
    {
        return _telemetry.stream().mapToInt((x) -> x.home()).summaryStatistics().getAverage();        
    }
    
    public double retail()
    {
        return _telemetry.stream().mapToInt((x) -> x.retail()).summaryStatistics().getAverage();
    }
    
    private LocalDate first() 
    {
        return _telemetry.stream().map((x) -> x.when()).sorted().findFirst().get();
    }
    
    private LocalDate last()
    {
        return _telemetry.stream().map((x) -> x.when()).sorted(Collections.reverseOrder()).findFirst().get();
    }
    
    public String toString()
    {
        String output = String.format("[ %s ] (%tF - %tF) Moving: %.2f / Retail %.2f / Home %.2f",
                _location, first(), last(), movement(), retail(), home());
        return output;
    }

    public String compareTo(Segment other)
    {
        double diffMovement = (this.movement() - other.movement())/other.movement() * 100;
        double diffHome = (this.home() - other.home()) / other.home() * 100;
        double diffRetail = (this.retail() - other.retail()) / other.retail() * 100;
        
        String output = String.format("Moving: %.2f%% / Retail %.2f%% / Home %.2f%%", diffMovement, diffRetail, diffHome);
        return output;
    }
}
