package org.eaves.mobility.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MobilityReading
{
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final Location _location;
    private final LocalDate _date;
    private final Telemetry _telemetry;

    public MobilityReading(String country, String region, String subRegion1, String subRegion2, 
                           String placeId, String date, int retail, int grocery, int parks, int transit,
                           int work, int resi)
    {
        _location = new Location(country, region, subRegion1, subRegion2, placeId);
        _date = LocalDate.parse(date, FORMATTER);
        _telemetry = new Telemetry(_date, retail, grocery, parks, transit, work, resi);
        
    }
    
    public Location location()
    {
        return _location;
    }

    public boolean isLocatedIn(Location location)
    {
        return _location.equals(location);
    }

    public Telemetry telemetry()
    {
        return _telemetry;
    }

    public boolean isInRange(LocalDate sd, LocalDate ed)
    {
        return !(_date.isAfter(ed) || _date.isBefore(sd));
    }
}
