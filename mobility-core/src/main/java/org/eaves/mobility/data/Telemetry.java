package org.eaves.mobility.data;

import java.time.LocalDate;
import java.util.Date;

public class Telemetry
{
    private final int _homeScore;
    private final int _moveScore;
    private final LocalDate _date;
    private final int _retail;

    public Telemetry(LocalDate when, int retail, int grocery, int parks, int transit, int work, int resi)
    {
        _date = when;
        _moveScore = 500 + retail + grocery + parks + transit + work;
        _homeScore = 100 + resi;
        _retail = 100 + retail;
    }
    
    public int movement()
    {
        return _moveScore;
    }
    
    public int home()
    {
        return _homeScore;
    }
    
    public int retail()
    {
        return _retail;
    }

    public LocalDate when()
    {
        return _date;
    }
}
