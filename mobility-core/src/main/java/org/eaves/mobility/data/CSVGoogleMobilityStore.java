package org.eaves.mobility.data;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CSVGoogleMobilityStore implements MobilityDataStore
{
    private final String _fileName;
    private final String _storeName;
    private boolean _loaded;
    private List<MobilityReading> _entries;

    public CSVGoogleMobilityStore(String fileName, String storeName)
    {
        _fileName = fileName;
        _storeName = storeName;
        _loaded = false;
        load(fileName);
    }

    private void load(String fileName)
    {
        InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);
        Reader r = new InputStreamReader(Objects.requireNonNull(is));

        CSVReader csv = new CSVReader(r);

        List<String[]> entries = new ArrayList<>();
        try {
            entries = csv.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("LineCount :" + entries.size());
        entries.remove(0);
        
        _entries = entries.stream().filter(item -> { return (item.length == 15); }).map(item -> {
            return converted(item);}).collect(Collectors.toList());
    }

    private MobilityReading converted(String[] item)
    {
        // System.out.println(Arrays.toString(item));
        return new MobilityReading(
                item[0], item[1], item[2], item[3], item[7],// location
                item[8],
                parseInt(item[9]), parseInt(item[10]), 
                parseInt(item[11]), parseInt(item[12]), 
                parseInt(item[13]), parseInt(item[14]));
    }
    
    private int parseInt(String s)
    {
        int rv = 0;
        try {
            rv = Integer.parseInt(s);
        }
        catch (NumberFormatException nfe)
        {
            // do nothing
        }
        return rv;
    }

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    
    @Override
    public Segment segment(Location location, String startDate, String endDate)
    {
        LocalDate sd = LocalDate.parse(startDate, FORMATTER);
        LocalDate ed = LocalDate.parse(endDate, FORMATTER);
        
        List<Telemetry> telemetry = 
                _entries.stream().
                        filter(item -> item.isLocatedIn(location)).
                        filter(item -> item.isInRange(sd,ed)).
                        map(item -> item.telemetry()).
                        collect(Collectors.toList());
        
        Segment rv = new Segment(location, telemetry);
        return rv;
    }

    @Override
    public List<Location> locations()
    {
        return  _entries.stream().map(item -> { return item.location(); }).collect(Collectors.toSet())
                .stream().collect(Collectors.toList());
    }

    @Override
    public Location location(String state, String region)
    {
        return locations().stream().filter(item -> item.matches(region)).findFirst().get();
    }

    @Override
    public Location place(String placeId)
    {
        return locations().stream().filter(item -> item.isPlace(placeId)).findFirst().get();
    }
    


}
