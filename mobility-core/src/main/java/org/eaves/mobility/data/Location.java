package org.eaves.mobility.data;

public class Location implements Comparable
{
    private final String _placeId;
    private final String _where;
    private final String _region;

    public Location(String country, String region, String subRegion1, String subRegion2, String placeId)
    {
        _region = subRegion1;
        _where = (subRegion2.trim().length() > 0) ? subRegion2 : "BLANK";
        _placeId = placeId;
    }
    
    public boolean matches(String location)
    {
        return _where.matches(location);
    }
    
    public String identity()
    {
        return _region + " / " + _where; 
    }
    
    public String toString()
    {
        return fullLocation();
    }

    public String fullLocation()
    {
        return identity() + " / " + _placeId;
    }

    @Override
    public int compareTo(Object o)
    {
        Location l = (Location) o;
        return _placeId.compareTo(l._placeId);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof Location)
        {
            return _placeId.equals(((Location) o)._placeId);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return _placeId.hashCode();
    }

    public boolean isPlace(String placeId)
    {
        return _placeId.equals(placeId);
    }
}
