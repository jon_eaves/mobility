package org.eaves.mobility.spike;

import com.opencsv.CSVReader;
import org.eaves.mobility.data.CSVGoogleMobilityStore;
import org.eaves.mobility.data.Location;
import org.eaves.mobility.data.MobilityDataStore;
import org.eaves.mobility.data.Segment;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CSVRead {
    public static void main(String[] args) {
        
        CSVRead run = new CSVRead();
        run.run(args);
    }
    
    public void run(String[] args)
    {
        // MobilityDataStore mds2020 = new CSVGoogleMobilityStore("2020_AU_Region_Mobility_Report.csv", "Australia 2020");
        MobilityDataStore mds2021 = new CSVGoogleMobilityStore("2021_AU_Region_Mobility_Report.csv", "Australia 2021");

        System.out.println(mds2021.locations().stream().filter(item -> item.matches(".*Mari.*")).collect(Collectors.toList()));
        System.out.println(mds2021.locations().stream().filter(item -> item.matches(".*North.*")).collect(Collectors.toList()));

        String syd = "Waverley Council";
        String mel = "City of Maribyrnong";
        
        Location sydney = mds2021.location("NSW", syd);
        Location melbourne = mds2021.location("Victoria", mel);
        Segment s_pre = mds2021.segment(sydney, "26-05-2021", "10-06-2021");
        Segment s = mds2021.segment(sydney, "26-06-2021", "28-06-2021");
        
        Segment m_pre = mds2021.segment(melbourne, "26-03-2021", "26-04-2021");
        Segment m = mds2021.segment(melbourne, "26-05-2021", "10-06-2021");        

        System.out.println(s_pre.toString());
        System.out.println(s.toString());
        System.out.println(s.compareTo(s_pre));
        System.out.println(m_pre.toString());
        System.out.println(m.toString());
        System.out.println(m.compareTo(m_pre));
        
    }

    public List<String[]> read(String name) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(name);
        Reader r = new InputStreamReader(Objects.requireNonNull(is));

        CSVReader csv = new CSVReader(r);

        List<String[]> entries = new ArrayList<>();
        try {
            entries = csv.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // System.out.println("LineCount :" + entries.size());

        return entries;
    }
}
